---
variables:
  GIT_STRATEGY: clone
  GSG_RELEASE_BRANCHES: "master"
  RELEASE_DESC: "HTTP load testing tool and library."
  RELEASE_VERSION: "v12.7.0"

.docker_definition: &docker_definition
  image: docker:stable
  services:
    - docker:stable-dind
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""

stages:
  - prepare
  - build
  - security
  - publish
  - release

.release_info: &release_info |
  # update release notes
  rm -f "${RELEASE_FILE}"
  mv build_info "${RELEASE_FILE}"
  cat "${RELEASE_FILE}"
  . "${RELEASE_FILE}"

#
# [prepare stage]
#
# Create:
# - "build_info" file used for the release commited in the repository
# - ".next-version" file with the next version bump to be commited
# - "CHANGELOG" file if needed
#

prepare:
  stage: prepare
  image: registry.gitlab.com/juhani/go-semrel-gitlab:v0.20.4
  script:
  - release test-git || true
  - release test-api
  - echo "${RELEASE_VERSION}" > .next-version
  - release changelog || true
  - echo "RELEASE_URL=https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/jobs/$CI_JOB_ID/artifacts/release" > build_info
  - echo "RELEASE_DESC=\"${RELEASE_DESC}\"" >> build_info
  - echo "RELEASE_SHA=$CI_COMMIT_SHA" >> build_info
  - echo "RELEASE_VERSION=$(<.next-version)" >> build_info
  - cat build_info
  - cat .next-version
  artifacts:
    paths:
    - build_info
    - .next-version
    - CHANGELOG.md
    expire_in: 1d
  except:
  - tags

#
# [build stage]
#
# Build docker images and store then in artifacts
# - master: create "application.tar"
#

.build_definition: &build_definition
  <<: *docker_definition
  stage: build
  before_script:
    - *release_info
  script: |
    docker build -t "$CI_REGISTRY_IMAGE:$RELEASE_VERSION" \
      --label "version=${RELEASE_VERSION}" \
      --label "commit_sha=${CI_COMMIT_SHA}" \
      --label "author=${CI_REGISTRY_USER}" \
      .

    docker save "$CI_REGISTRY_IMAGE:$RELEASE_VERSION" > ${DOCKER_TAR_NAME}

    # needed for clair-scanner
    docker login -u ${CI_REGISTRY_USER} -p ${CI_JOB_TOKEN} ${CI_REGISTRY}
    docker tag "$CI_REGISTRY_IMAGE:$RELEASE_VERSION" "${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_SLUG}:${CI_COMMIT_SHA}"
    docker push "${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_SLUG}:${CI_COMMIT_SHA}"

build:stable:
  <<: *build_definition
  variables:
    DOCKER_TAR_NAME: application.tar
    RELEASE_FILE: release_info
  except:
  - tags
  artifacts:
    paths:
      - application.tar
    expire_in: 1d

#
# clair
# (https://gitlab.com/gitlab-org/gitlab-ce/blob/master/lib/gitlab/ci/templates/Security/Container-Scanning.gitlab-ci.yml)
#

container_scanning:
  stage: security
  image: docker:stable
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""
    # Defining two new variables based on GitLab's CI/CD predefined variables
    # https://docs.gitlab.com/ee/ci/variables/#predefined-environment-variables
    CI_APPLICATION_REPOSITORY: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG
    CI_APPLICATION_TAG: $CI_COMMIT_SHA
    # Prior to this, you need to have the Container Registry running for your project and setup a build job
    # with at least the following steps:
    #
    # docker build -t $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG .
    # docker push $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
    #
    # Container Scanning deals with Docker images only so no need to import the project's Git repository:
    GIT_STRATEGY: none
    # Services and containers running in the same Kubernetes pod are all sharing the same localhost address
    # https://docs.gitlab.com/runner/executors/kubernetes.html
    DOCKER_SERVICE: docker
    DOCKER_HOST: tcp://${DOCKER_SERVICE}:2375/
    # https://hub.docker.com/r/arminc/clair-local-scan/tags
    CLAIR_LOCAL_SCAN_VERSION: v2.0.8_0ed98e9ead65a51ba53f7cc53fa5e80c92169207
    CLAIR_EXECUTABLE_VERSION: v12
    CLAIR_EXECUTABLE_SHA: 44f2a3fdd7b0d102c98510e7586f6956edc89ab72c6943980f92f4979f7f4081
    ## Disable the proxy for clair-local-scan, otherwise Container Scanning will
    ## fail when a proxy is used.
    NO_PROXY: ${DOCKER_SERVICE},localhost
  allow_failure: true
  services:
    - docker:stable-dind
  script:
    - if [[ -n "$KUBERNETES_PORT" ]]; then { export DOCKER_SERVICE="localhost" ; export DOCKER_HOST="tcp://${DOCKER_SERVICE}:2375" ; } fi
    - |
      if [[ -n "$CI_REGISTRY_USER" ]]; then
        echo "Logging to GitLab Container Registry with CI credentials..."
        docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
        echo ""
      fi
    - docker run -d --name db arminc/clair-db:latest
    - docker run -p 6060:6060 --link db:postgres -d --name clair --restart on-failure arminc/clair-local-scan:${CLAIR_LOCAL_SCAN_VERSION}
    - apk add -U wget ca-certificates
    - docker pull ${CI_APPLICATION_REPOSITORY}:${CI_APPLICATION_TAG}
    - wget https://github.com/arminc/clair-scanner/releases/download/${CLAIR_EXECUTABLE_VERSION}/clair-scanner_linux_amd64
    - echo "${CLAIR_EXECUTABLE_SHA}  clair-scanner_linux_amd64" | sha256sum -c
    - mv clair-scanner_linux_amd64 clair-scanner
    - chmod +x clair-scanner
    - touch clair-whitelist.yml
    - retries=0
    - echo "Waiting for clair daemon to start"
    - while( ! wget -T 10 -q -O /dev/null http://${DOCKER_SERVICE}:6060/v1/namespaces ) ; do sleep 1 ; echo -n "." ; if [ $retries -eq 10 ] ; then echo " Timeout, aborting." ; exit 1 ; fi ; retries=$(($retries+1)) ; done
    - ./clair-scanner -c http://${DOCKER_SERVICE}:6060 --ip $(hostname -i) -r gl-container-scanning-report.json -l clair.log -w clair-whitelist.yml ${CI_APPLICATION_REPOSITORY}:${CI_APPLICATION_TAG} || true
  artifacts:
    reports:
      container_scanning: gl-container-scanning-report.json
  dependencies: []
  only:
    refs:
      - branches
    variables:
      - $GITLAB_FEATURES =~ /\bcontainer_scanning\b/
  except:
    variables:
      - $CONTAINER_SCANNING_DISABLED

#
# [publish stage]
#
# Publish images to public Docker registry
#
# master
# - automatic published

.publish_definition: &publish_definition
  <<: *docker_definition
  stage: publish
  allow_failure: false

publish:release:
  <<: *publish_definition
  variables:
    DOCKER_TAR_NAME: application.tar
  script:
    - echo "Logging to ${REGISTRY_DOCKER} Container Registry with CI credentials..."
    - docker login -u "${REGISTRY_DOCKER_USER}" -p "${REGISTRY_DOCKER_PASS}" "${REGISTRY_DOCKER}"
    - docker load -i ${DOCKER_TAR_NAME}
    - RELEASE_VERSION=$(cat .next-version)
    - echo "${RELEASE_VERSION}"
    - docker tag "$CI_REGISTRY_IMAGE:$RELEASE_VERSION" "araulet/vegeta:$RELEASE_VERSION"
    - docker push "araulet/vegeta:$RELEASE_VERSION"
    - docker tag "$CI_REGISTRY_IMAGE:$RELEASE_VERSION" "araulet/vegeta:latest"
    - docker push "araulet/vegeta:latest"
  only:
    - master

#
# [release stage]
#

#
# master
# - create new release notes
# - commit changelog, release note
# - tagged the branch
#

.release_info_definition: &release_info_definition
  image: registry.gitlab.com/juhani/go-semrel-gitlab:v0.20.4
  stage: release
  when: on_success
  before_script:
    - ls -la # debug purpose
    - *release_info

release:
  <<: *release_info_definition
  dependencies:
  - prepare
  - publish:release
  variables:
    RELEASE_FILE: release_info
    DOCKER_TAR_NAME: application.tar
  script: |
    release test-git && {
      echo "[YES] some changes have been detected."
      release commit-and-tag "${RELEASE_FILE}" CHANGELOG.md
    } || {
      echo "[NO] no changes have been detected."
    }
  only:
  - master
