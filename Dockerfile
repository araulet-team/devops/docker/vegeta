############################
# STEP 1 build executable binary
############################
FROM golang:alpine AS builder

ENV VEGETA_VERSION="v12.7.0"

RUN apk update && apk add --no-cache git
RUN GO111MODULE=on go get -u "github.com/tsenart/vegeta@${VEGETA_VERSION}"
RUN vegeta -version
RUN adduser -D -g '' gouser


############################
# STEP 2 build a small image
############################
FROM alpine:latest

COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /go/bin/vegeta /go/bin/vegeta

ENV PATH="/go/bin/:${PATH}"

USER gouser

CMD ["vegeta", "-help"]
