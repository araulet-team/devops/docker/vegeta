# [vegeta](https://github.com/tsenart/vegeta)

Minimal Docker image for HTTP load testing tool and library.

https://gitlab.com/araulet-team/devops/docker/vegeta

## Usage

```shell
$ docker run \
-it \
--rm \
--name vegeta araulet/vegeta \
vegeta -version
```

## Versions (https://cloud.docker.com/repository/docker/araulet/vegeta)

* v12.7.0
